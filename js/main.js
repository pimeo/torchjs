
;(function($) {

	$.Torch = function (el, options){

		 // To avoid scope issues, use 'base' instead of 'this' to reference this
        // class from internal events and functions.
        var base = this;

        // Access to jQuery and DOM versions of element.
        base.$el = $(el);
        base.el = el;

        // CREATE OUTERMOST PORTIONS
        function createExtremityDiv(){
        	var _area = $('<div />')
                    .attr({
                        'id': 'area-' + base.index,
                        'class': 'torch-area',
                    });

    		_area
    			.css(base.defaultTorchArea)
				.css( getExtremityCSSStyle() );

    		base.$el.append(_area);

            
        };

        function getExtremityCSSStyle(){
        	var tmp = {};

        	if( base.isSpriteCustomDim ){
        		switch( base.index ){
        			case 1:
        			case 3:
        			case 7:
        			case 9:
        				tmp = {
        					width: base.gridLeft,
				    		height: base.gridTop,
		    				left: (base.index == 3 || base.index == 9 ) ? ( base.gridLeft + base.options.spriteWidth) : (base.lines * base.gridLeft),
		    				top : (base.index == 7 || base.index == 9 ) ? ( base.gridTop + base.options.spriteHeight) : (base.columns * base.gridTop)
        				};
        				break;
        			case 2:
        			case 8:
        				tmp = {
        					width: base.options.spriteWidth,
				    		height: base.gridTop,
		    				left: (base.lines * base.gridLeft),
		    				top : (base.index == 8) ? ( base.gridTop + base.options.spriteHeight) : base.columns * base.gridTop  
        				};
        				break;
        			case 4:
        			case 6:
        				tmp = {
        					width: base.gridLeft,
				    		height: base.options.spriteHeight,
		    				left: ( base.index == 6 ) ? ( base.gridLeft + base.options.spriteWidth) : (base.lines * base.gridLeft),
		    				top : base.columns * base.gridTop  
        				};
        				break;
        			default: 
        		}
        	}else{
        		tmp = {
        			width: base.gridLeft,
		    		height: base.gridTop,
    				left: (base.lines * base.gridLeft),
    				top : base.columns * base.gridTop  
        		}
        	}

        	//tmp.backgroundColor = background: '#000000';
            tmp.opacity = base.options.containerOpacity;

        	return tmp;


        }

        // CREATE CENTER CENTER TORCH DIV
        function createCenterDiv(){

        	var _area = $('<div />').attr({
    			'id': 'area-' + base.index,
    			'class': 'torch-area-center'
    		});


        	base.options.spriteWidth = (base.options.spriteWidth != 0) ? base.options.spriteWidth : base.gridLeft;
			base.options.spriteHeight = (base.options.spriteHeight != 0) ? base.options.spriteHeight : base.gridTop;

			base.options.spritePivotX = base.options.spriteWidth/2;
			base.options.spritePivotY = base.options.spriteHeight/2;

    		_area
    			.css(base.defaultTorchArea)
    			.css({
    				width: base.gridLeft,
		    		height: base.gridTop,
    				left: (base.lines * base.gridLeft),
    				top : base.columns * base.gridTop,
    				width: base.options.spriteWidth ,
    				height: base.options.spriteHeight,
    				//'background-color':'transparent',
    				'z-index': 10,
    		});


    		base.$el.append(_area);
        };


        // CREATE TORCH STRUCTURE
        function createTorchArea(){
        	for (var i = 1; i <= base.totalDivs; i++){

        		if( base.lines % 3 == 0 ){
        			base.lines = 0;
        			if( i != 1 ) base.columns++;
        		}

        		// center center of the grid
        		if( i == 5 ) createCenterDiv();
        		else createExtremityDiv();
        		
        		base.lines++;
        		base.index++;

        	}
        };

        // define default grid area dimensions
        function defineGridAreaDimentions(){

        	// if we use custom sprite dimension
        	if( base.isSpriteCustomDim ){
        		// Third part dimensions of container
	        	base.gridLeft = Math.ceil( ( base.options.containerWidth - base.options.spriteWidth ) / 2);
	        	base.gridTop =  Math.ceil( ( base.options.containerHeight - base.options.spriteHeight ) / 2);
        	}else{
        		// Third part dimensions of container
				base.gridLeft = Math.ceil( base.options.containerWidth / 3);
        		base.gridTop = Math.ceil( base.options.containerHeight/ 3 );
        	}

        }

        // Manage Mousen, Touch and Resize Events
        function manageEvents(){

            // -- we are ready
            base.$el.trigger('onReady.Torch');

            // -- resize
            if( !base.isSpriteCustomDim) $(window).on('resize', resizeTorch);

            // -- touch and mouse
        	var centerDiv = document.getElementsByClassName('torch-area-center')[0];

        	if( base.isMobile.any() != null ){
        		// tablet move
        		centerDiv.addEventListener('touchmove', moveTorchSprite, false);
        	}else{
        		//$(window).on('mousemove', moveTorchSprite);
                // Mouse move for all web browsers
                try
                {
                    document.addEventListener('mousemove', moveTorchSprite, false);
                }catch(e)
                {
                    document.attachEvent('onmousemove', moveTorchSprite);
                }
        	}
        }

        // update all extremity div area as torch area positions
        // width, height, left and top
        function updateExtremityArea(){
        	for (var i = 1; i <= base.totalDivs; i++){

        		var tmp = {};

        		switch(i){
        			case 1:
        				tmp = { left: 0, top: 0, width: base.centerOffsetLeft, height: base.centerOffsetTop }
	        			break;
	        		case 2:
        				tmp = { left: base.centerOffsetLeft, top:0, width: base.options.spriteWidth, height: base.centerOffsetTop };
	        			break;
	        		case 3:
	        			tmp = { 
	        				left: base.centerOffsetLeft + base.options.spriteWidth,
	        				top:0, 
	        				width: (base.options.containerWidth - base.options.spriteWidth - base.centerOffsetLeft),
	        				height: base.centerOffsetTop
	        			};
	        			break;
	        		case 4:
	        			tmp = { 
	        				left: 0,
	        				top: base.centerOffsetTop, 
	        				width: base.centerOffsetLeft,
	        				height: base.options.spriteHeight
	        			};
	        			break;
	        		// NOTHING FOR CENTER TORCH AREA
	        		case 5:
	        			break;
	        		case 6:
	        			tmp = { 
	        				left: base.centerOffsetLeft + base.options.spriteWidth,
	        				top: base.centerOffsetTop, 
	        				width: (base.options.containerWidth - base.options.spriteWidth - base.centerOffsetLeft),
	        				height: base.options.spriteHeight
	        			};
	        			break;
	        		case 7:
	        			tmp = { 
	        				left: 0,
	        				top: (base.options.spriteHeight + base.centerOffsetTop), 
	        				width: base.centerOffsetLeft,
	        				height: (base.options.containerHeight - base.options.spriteHeight - base.centerOffsetTop)
	        			};
	        			break;
	        		case 8:
	        			tmp = { 
	        				left: base.centerOffsetLeft,
	        				top: (base.options.spriteHeight + base.centerOffsetTop), 
	        				width: base.options.spriteWidth,
	        				height: (base.options.containerHeight - base.options.spriteHeight - base.centerOffsetTop)
	        			};
	        			break;
	        		case 9:
	        			tmp = { 
	        				left: base.centerOffsetLeft + base.options.spriteWidth,
	        				top: (base.options.spriteHeight + base.centerOffsetTop), 
	        				width: (base.options.containerWidth - base.options.spriteWidth - base.centerOffsetLeft),
	        				height: (base.options.containerHeight - base.options.spriteHeight - base.centerOffsetTop)
	        			};
	        			break;
	        		default: //console.log('ERROR UPDATE AREA CSS ATTRIBUTES');

        		}
        		
        		$('#area-' + i).css( tmp );
        	}
        }

        function debugPlugin(){
        	if( base.options.debug ){
        		$('body').append("<div class=\""+base.options.debugClass+"\"></div>");
        		$('.' + base.options.debugClass).append("<div class=\"touchControls\"><h6>TOUCH EVENT</h6><div class=\"touchX\">X : <span></span></div><div class=\"touchY\">Y : <span></span></div></div>");
        	}
        }

        function setDebugTouch(posX, posY){
        	//console.log('here');
        	$('.' + base.options.debugClass + ' .touchX span').html(posX); 
        	$('.' + base.options.debugClass + ' .touchY span').html(posY); 
        }


        // handle X and Y positions of mouse
        function moveTorchSprite(e){

        	if(e) (e.preventDefault) ? e.preventDefault() : e.returnValue = false;

        	//console.log(e.pageX, e.pageY);
        	var tmp = {};

        	// depends if we are in mobile or tablet
        	if (base.isMobile.any() != null){

        		//var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];

        		var touch = e.changedTouches;

        		base.currentPosX = touch[0].pageX;
        		base.currentPosY = touch[0].pageY;

        	}else{
        		if( e != null ){
                    base.currentPosX = e.pageX;
                    base.currentPosY = e.pageY;
                }else{
                    console.log('here');
                }
        	}

        	if( base.options.debug ) setDebugTouch(base.currentPosX, base.currentPosY);
        	
        	base.centerOffsetTop = parseInt($('.torch-area-center').offset().top);
        	base.centerOffsetLeft = parseInt($('.torch-area-center').offset().left);

        	if ( (base.currentPosX ) < base.options.containerWidth && (base.currentPosX ) >= 0 )
				tmp.left = (base.isSpriteCustomDim) ? base.currentPosX - base.options.spritePivotX : base.currentPosX - ( base.gridLeft /2 );  
			else tmp.left = base.centerOffsetLeft

			if ( (base.currentPosY) < base.options.containerHeight && (base.currentPosY ) >= 0 )
				tmp.top = (base.isSpriteCustomDim) ? base.currentPosY - base.options.spritePivotY : base.currentPosY- ( base.gridTop /2 )
			else tmp.top = base.centerOffsetTop
       		
			// associate pivotX and pivotY to torch area center as our mouse positions
        	$('.torch-area-center').css(tmp);

        	updateExtremityArea();
        }

        function readyTorch(){
            // -- trigger end overlay screen
            if( base.options.startOnFading ) removeOverlayScreen();

            // -- custom plugin callback
            if( base.options.onReadyTorch) base.options.onReadyTorch();
        }

        function removeTorchEvents(){
            if( base.isMobile.any() != null ){
                // tablet move
                var centerDiv = document.getElementsByClassName('torch-area-center')[0];
                centerDiv.removeEventListener('touchmove', moveTorchSprite, false);
            }else{
                // Mouse move for all web browsers
                try
                {
                    document.removeEventListener('mousemove', moveTorchSprite, false);
                }catch(e)
                {
                    document.detachEvent('onmousemove', moveTorchSprite);
                }
            }
        }

        // tablets and smartphones detection
        function detectMobile(){

        	base.isMobile = {
			    Android: function() {
			        return navigator.userAgent.match(/Android/i);
			    },
			    BlackBerry: function() {
			        return navigator.userAgent.match(/BlackBerry/i);
			    },
			    iOS: function() {
			        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			    },
			    Opera: function() {
			        return navigator.userAgent.match(/Opera Mini/i);
			    },
			    Windows: function() {
			        return navigator.userAgent.match(/IEMobile/i);
			    },
			    any: function() {
			        return (base.isMobile.Android() || base.isMobile.BlackBerry() || base.isMobile.iOS() || base.isMobile.Opera() || base.isMobile.Windows());
			    }
			};
        }

        // overlay element over our torch
        function createOverlayScreen(){
        	base.$overlay = $('<div />', base.options.overlay);
            base.$overlay
                    .css(base.defaultTorchArea)
                    .css({
                        width: base.options.containerWidth,
                        height: base.options.containerHeight
            })
            base.$el.append(base.$overlay);
        }

        // overlay screen remove on fadeOut
        function removeOverlayScreen(){
            setTimeout(function(){
                base.$overlay.fadeOut(base.options.overlayFadeDelay);
            }, base.options.overlayStartDelay);
        }

        // remove completely torch selector
        function removeTorch(e){
            if(e) (e.preventDefault) ? e.preventDefault() : e.returnValue = false;
            base.$el.remove();
            if( base.options.generateControls ) base.$actions.remove();
            if( base.options.onRemoveTorch ) base.options.onRemoveTorch();
        }

        // hide only torch showing the overlay element 
        function hideTorch(e){
            if(e) (e.preventDefault) ? e.preventDefault() : e.returnValue = false;
            base.$overlay.fadeIn(base.options.overlayFadeDelay);
            if( base.options.onShowTorch ) base.options.onHideTorch();
        }

        // show only torch hiding the overlay element
        function showTorch(e){
            if(e) (e.preventDefault) ? e.preventDefault() : e.returnValue = false;
            base.$overlay.fadeOut(base.options.overlayFadeDelay);
            if( base.options.onShowTorch ) base.options.onShowTorch();
        }

        // create torch controls (delete/hide/show)
        function createTorchControls(){
            base.$actions = $('<div />', {class: base.options.torchControlClass}).appendTo(base.$el.parent());
            base.$removeTorch = $("<a href=\"#\" class=\"remove-torch\">Supprimer Torche</a>");
            base.$hideTorch = $("<a href=\"#\" class=\"hide-torch\">Cacher Torche</a>");
            base.$displayTorch = $("<a href=\"#\" class=\"display-torch\">Afficher Torche</a>");

            // -- add to dom
            base.$actions.append(base.$removeTorch);
            base.$actions.append(base.$hideTorch);
            base.$actions.append(base.$displayTorch);

            // -- events
            base.$removeTorch.on('click', removeTorch);
            base.$hideTorch.on('click', hideTorch);
            base.$displayTorch.on('click', showTorch);
        }

        function resizeTorch(){
            console.log('resize');
            // -- resize container
            base.options.containerWidth = $(window).width();
            base.options.containerHeight = $(window).height();

             // prevent width and height to our selector
            base.$el.css({
                width: base.options.containerWidth,
                height: base.options.containerHeight 
            });

            defineGridAreaDimentions();
            updateExtremityArea();
            moveTorchSprite(null);

        }

         // INIT
        base.init = function() {

        	base.options = $.extend({}, $.Torch.defaultOptions, options);

        	// Default params for torch grid
		    base.totalDivs  = 9;
		    base.index = 1;
		    base.lines = 0;
		    base.columns = 0;

		    // mouse position
		    base.currentPosX = 0;
		    base.currentPosY = 0;

		    base.defaultTorchArea = {
		    	position: 'absolute',
		    	left: 0,
		    	top: 0
		    };

		    // prevent width and height to our selector
		    base.$el.css({
		    	width: base.options.containerWidth,
		    	height: base.options.containerHeight 
		    });

            // -- BINDINGS
            if( base.options.onReadyTorch ){
                 base.$el.bind('onReady.Torch', readyTorch);
            }

		    // touch event position debug
		    debugPlugin();

		    if( base.options.startOnFading ) createOverlayScreen();

		    detectMobile();

		    // bool which defines if we are custom 
		    // dimension for the center of the torch
		    base.isSpriteCustomDim = (base.options.spriteHeight != 0 || base.options.spriteWidth != 0 ) ? true : false;

            // define Torch bounds
		    defineGridAreaDimentions();

        	// create Torch Area
        	createTorchArea();

            if( base.options.generateControls ) createTorchControls();

        	// manage mouse events
        	manageEvents();

        };

        base.init();

	};

	// Sets the option defaults.
    $.Torch.defaultOptions = {
        spriteWidth: 0,
        spriteHeight: 0,
        containerHeight: $(window).height(),
        containerWidth: $(window).width(),
        containerOpacity: 1,

        generateControls: true,
        torchControlClass: 'torch-controls',

        debugClass: 'debug',
        debug: false,

        startOnFading: true,

        overlay: {
        	'class': 'torch-overlay'
        },
        overlayStartDelay: 1000,
        overlayFadeDelay:  600,

        // deleting torch plugin
        onRemoveTorch: function() {},
        // show torch
        onShowTorch: function() {},
        // hide torch
        onHideTorch: function() {},
        // ready
        onReadyTorch: function() {},
    };

    // Returns enhanced elements that will fix to the top of the page when the
    // page is scrolled.
    $.fn.Torch = function(options) {
        return this.each(function() {
            (new $.Torch(this, options));
        });
    };



})(jQuery);